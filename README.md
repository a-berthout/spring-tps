## Fil rouge

Le fil rouge consiste à développer un quiz où plusieurs joueurs peuvent s'affronter.

Même si le jeu est simple, les participants à la formation n'auront pas le temps de tout développer. Une partie du jeu (les APIs et le moteur de base sont fournis).

A eux de suivre les TPs pour que le jeu puisse fonctionner et évoluer.

## Découpage des projets

- `game-core` : code pour les premiers TPs sur Spring Core
  - Modifié par les participants
- `game-web` : code pour les TPs avec Spring Web
  - Les participants crééent un nouveau projet avec Spring Boot. Ce projet sert pour les solutions


## Enoncés


### Branches

La formation Spring propose plusieurs variantes :
- Configuration en Java
- Configuration en XML
- Configuration Mixte (TODO)


Les énoncés sont disponible sur des branches particulières suivant la nomenclature suivante :
```
enonce/<variante>
```

Exemple :
```
enonce/conf-java
```


### Générer pour les participants en PDF

Sur le projet `game-tp-enonces`, exécuter la commande Maven :
```
mvn post-site
```

Les PDFs sont générés dans le dossier `target/generated-docs` du projet `game-tp-enonces`.

### Générer pour le formateur en PDF

Sur le projet `game-tp-enonces`, exécuter la commande Maven :
```
mvn post-site -Pformateur
```

Les PDFs sont générés dans le dossier `target/generated-docs` du projet `game-tp-enonces`.

## Code

Chaque TP a une branche pour l'état initial et une branche pour la solution.


### Branches

La nomenclature pour une branche initiale est la suivante :
```
tp/<variante>/<numero étape>-<nom>
```

Exemple :
```
tp/conf-java/1-concepts-inj
```

Les solutions sotn systématiquement présentes dans la branche suivante

Chaque énoncé de TP doit donner (au moins pour le formateur) la branche initiale à utiliser. Le but étant que les participants bloqués à une étape ne le soient pas sur tout le fil rouge.



