package fr.sii.formation.quiz.core.service;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Round;

public interface DisplayService {
	/**
	 * Display the current game state 
	 * 
	 * @param game
	 *            the game to display
	 */
	void display(Game game);

	/**
	 * Display the reponses
	 * 
	 * @param game
	 *            the current game
	 * @param rounds
	 *            the rounds
	 */
	void display(Game game, Round round);
}
