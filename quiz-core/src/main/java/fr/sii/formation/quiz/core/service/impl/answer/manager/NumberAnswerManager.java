package fr.sii.formation.quiz.core.service.impl.answer.manager;

import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.QuestionType;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.service.answer.AnswerManager;

public class NumberAnswerManager implements AnswerManager {

	@Override
	public QuestionType getManagedType() {
		return QuestionType.INTEGER;
	}

	@Override
	public boolean isStrictlyCorrect(Question question, Response response) {
		return response.getNumber() != null && Long.parseLong(question.getAnswer().getText()) == response.getNumber();
	}

	@Override
	public boolean isAlmostCorrect(Question question, Response response) {
		return response.getNumber() != null && (
			   (Long.parseLong(question.getAnswer().getText()) - 5) <= response.getNumber() ||
			   (Long.parseLong(question.getAnswer().getText()) + 5) >= response.getNumber() );
	}

}
