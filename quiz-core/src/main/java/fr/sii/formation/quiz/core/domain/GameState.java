package fr.sii.formation.quiz.core.domain;

public enum GameState {
	READY,
	PLAYING,
	WAITING,
	FINISHED;
}
