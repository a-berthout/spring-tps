package fr.sii.formation.quiz.core.service.exception;

public class PromptException extends GameException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1848605516756703632L;

	public PromptException(String message, Throwable cause) {
		super(message, cause);
	}

	public PromptException(String message) {
		super(message);
	}

	public PromptException(Throwable cause) {
		super(cause);
	}

}
