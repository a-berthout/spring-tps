package fr.sii.formation.quiz.core.service;

import java.util.List;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.service.exception.NoQuestionException;

public interface QuestionService {

	/**
	 * Retrieves all available questions of the game
	 * 
	 * @param game
	 * @return available questions
	 */
	public List<Question> getAvailableQuestions(Game game);


	/**
	 * Retrieves the next question to ask
	 * 
	 * @param game
	 * @return next question in the list if exists
	 * @throws NoQuestionException
	 */
	public Question getNextQuestion(Game game) throws NoQuestionException;

}
