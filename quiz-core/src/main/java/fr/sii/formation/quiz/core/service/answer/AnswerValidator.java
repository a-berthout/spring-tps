package fr.sii.formation.quiz.core.service.answer;

import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.Response;

public interface AnswerValidator {

	boolean isCorrect(Question question, Response response);

}
