package fr.sii.formation.quiz.core.service.impl;

import fr.sii.formation.quiz.core.config.GameProperties;
import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.GameState;
import fr.sii.formation.quiz.core.service.DisplayService;
import fr.sii.formation.quiz.core.service.GameService;
import fr.sii.formation.quiz.core.service.exception.GameException;

public class SimpleGameService implements GameService {
	private SimpleGameRunner gameRunner;
	private DisplayService displayService;
	private GameProperties gameOptions;

	public SimpleGameService(SimpleGameRunner gameRunner, DisplayService displayService, GameProperties gameOptions) {
		super();
		this.gameRunner = gameRunner;
		this.displayService = displayService;
		this.gameOptions = gameOptions;
	}

	@Override
	public void start(Game game) throws GameException {
		if (null == game.getPlayers() || game.getPlayers().size() < gameOptions.getMinPlayers()) {
			throw new GameException("Not enough players, min is " + gameOptions.getMinPlayers());
		}
		if (null == game.getQuestions() || game.getQuestions().size() < gameOptions.getMinQuestions()) {
			throw new GameException("Not enough questions, min is " + gameOptions.getMinQuestions());
		}
		game.setState(GameState.PLAYING);
		displayService.display(game);
		gameRunner.next(game);
	}

	@Override
	public void stop(Game game) throws GameException {
		game.setState(GameState.FINISHED);
	}

}
