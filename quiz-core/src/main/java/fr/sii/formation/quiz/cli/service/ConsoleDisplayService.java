package fr.sii.formation.quiz.cli.service;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Player;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.domain.Round;
import fr.sii.formation.quiz.core.service.DisplayService;

public class ConsoleDisplayService implements DisplayService {
	
	@Override
	public void display(Game game) {
		System.out.println();
		System.out.println("=======================================/  ROUND "+ game.getCurrentRound()+"  \\=======================================");
		for(Player player : game.getPlayers()) {
			System.out.println(player);
		}
		System.out.println();
		
		int remainingQuestions = game.getQuestions().size() - game.getCurrentRound();
		
		if (remainingQuestions > 0) {
			System.out.println(remainingQuestions + " questions restantes !");
			System.out.println();
			System.out.println("===========================================================================================");
			System.out.println();
		} else {
			System.out.println("===========================================================================================");
			System.out.println();
			System.out.println("                                        FIN DU JEU");
			System.out.println();
			System.out.println("===========================================================================================");
		}
	}

	@Override
	public void display(Game game, Round round) {
		System.out.println("=======================================/  REPONSE  \\=======================================");
		System.out.println("La bonne réponse était : " + round.getQuestion().getAnswer().getText());
		for(Response response : round.getResponses()) {
			System.out.println(response);
		}
	}

}
