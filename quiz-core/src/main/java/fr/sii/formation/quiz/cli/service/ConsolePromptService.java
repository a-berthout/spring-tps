package fr.sii.formation.quiz.cli.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Player;
import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.QuestionType;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.service.PromptService;
import fr.sii.formation.quiz.core.service.exception.GameException;
import fr.sii.formation.quiz.core.service.exception.PromptException;

public class ConsolePromptService implements PromptService {

	@Override
	public Response ask(Game game, Player player, Question question) throws PromptException {
		try {
			return askQuestion(player, question);
		} catch(IOException e) {
			throw new PromptException("Can't read the console", e);
		} catch (GameException e) {
			throw new PromptException("Action couldn't be played", e);
		}
	}

	private Response askQuestion(Player player, Question question) throws IOException, PromptException {
		System.out.println("Question : "+question.getText());
		System.out.println("-> Choisissez une réponse pour le joueur :  "+player.getName());
		String choice = null;
		while(choice==null || !isValidChoice(question, choice)) {
			choice = readConsole();
			if(isValidChoice(question, choice)) {
				return getValidResponse(question, player, choice);
			} else {
				System.err.println("    Choix non valide "+choice);
			}
		}
		throw new PromptException("Invalid action choice");
	}

	private boolean isValidChoice(Question question, String choice) {
		// @formatter:off
		return !choice.isEmpty() && 
				(QuestionType.SENTENCE.equals(question.getQuestionType()) || choice.matches("^-?\\d+$"));
		// @formatter:on
	}

	private Response getValidResponse(Question question, Player player, String choice) {
		// @formatter:off
		Response response = new Response();
		response.setPlayer(player);
		if (QuestionType.SENTENCE.equals(question.getQuestionType())) {
			response.setText(choice);
		} else {
			response.setNumber(Long.parseLong(choice));
		}
		return response;
		// @formatter:on
	}
	
	private String readConsole() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		return br.readLine();
	}
}
