package fr.sii.formation.quiz.core.service.exception;

public class NoQuestionException extends GameException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 84561673803569976L;

	public NoQuestionException(String message) {
		super(message);
	}

	public NoQuestionException(Throwable cause) {
		super(cause);
	}

	public NoQuestionException(String message, Throwable cause) {
		super(message, cause);
	}

}