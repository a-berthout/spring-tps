package fr.sii.formation.quiz.core.service.answer;

import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.QuestionType;
import fr.sii.formation.quiz.core.domain.Response;

public interface AnswerManager {

	public QuestionType getManagedType();

	public boolean isStrictlyCorrect(Question question, Response response);

	public boolean isAlmostCorrect(Question question, Response response);

}
