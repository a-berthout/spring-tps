package fr.sii.formation.quiz.core.service;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.service.exception.GameException;

/**
 * 
 * @author nroussel
 *
 */
public interface GameService {
	/**
	 * Start the game
	 * 
	 * @param game
	 *            the game to start
	 * @throws GameException
	 *             when the game couldn't start or already started
	 */
	public void start(Game game) throws GameException;

	/**
	 * Stop the game
	 * 
	 * @param game
	 *            the game to stop
	 * @throws GameException
	 *             when the game couldn't stop or already stopped
	 */
	public void stop(Game game) throws GameException;

}
