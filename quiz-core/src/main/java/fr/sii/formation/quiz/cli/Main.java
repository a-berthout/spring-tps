package fr.sii.formation.quiz.cli;

import java.util.Arrays;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Player;
import fr.sii.formation.quiz.core.service.GameService;
import fr.sii.formation.quiz.core.service.exception.GameException;

public class Main {

	public static void main(String[] args) throws GameException {
		GameService gameService = init();
		// create the empty game
		Game game = new Game(true);
		// set game Id
		game.setId(1L);
		// set player
		game.setPlayers(Arrays.asList(new Player(1L, "John Doe"), new Player(2L, "Jane McDoe")));
		// start the game
		gameService.start(game);
	}

	private static GameService init() {
		// TP2 : utiliser Spring
		
	}
	
	/*
	 * Solution du TP1 (pour information)
	 * 
	private static GameService initTP1() {
		AnswerManager unknownManager = new UnknownAnswerManager();
		AnswerManager stringManager = new StringAnswerManager();
		AnswerManager numerManager = new NumberAnswerManager();
		
		AnswerValidator answerValidator = new StrictValidator(unknownManager, stringManager, numerManager);
		
		RoundService roundService = new SimpleRoundService();
		
		QuestionService questionService = new SimpleQuestionService();
		DisplayService displayService = new ConsoleDisplayService();
		PromptService promptService = new ConsolePromptService();
		
		SimpleGameRunner gameRunner = new SimpleGameRunner(questionService, promptService, displayService, roundService, answerValidator);
		
		return new SimpleGameService(gameRunner, displayService, new GameProperties());
	}
	*/

}
