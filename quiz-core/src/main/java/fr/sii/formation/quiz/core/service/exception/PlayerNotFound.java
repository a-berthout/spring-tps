package fr.sii.formation.quiz.core.service.exception;

public class PlayerNotFound extends GameException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 43673803569976L;

	public PlayerNotFound(String message) {
		super(message);
	}

	public PlayerNotFound(Throwable cause) {
		super(cause);
	}

	public PlayerNotFound(String message, Throwable cause) {
		super(message, cause);
	}

}