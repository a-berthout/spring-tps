package fr.sii.formation.quiz.core.service.impl.answer;

import java.util.Arrays;
import java.util.List;

import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.service.answer.AnswerManager;
import fr.sii.formation.quiz.core.service.answer.AnswerValidator;

public class StrictValidator implements AnswerValidator {

	private final List<AnswerManager> answerManagers;

	public StrictValidator(AnswerManager... answerManagers) {
		this(Arrays.asList(answerManagers));
	}

	public StrictValidator(List<AnswerManager> answerManagers) {
		super();
		this.answerManagers = answerManagers;
	}
	
	@Override
	public boolean isCorrect(Question question, Response response) {
		for (AnswerManager manager : answerManagers) {
			if (question.getQuestionType().equals(manager.getManagedType())) {
				return manager.isStrictlyCorrect(question, response);
			}
		}
		return false;
	}

}
