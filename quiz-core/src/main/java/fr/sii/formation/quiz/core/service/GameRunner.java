package fr.sii.formation.quiz.core.service;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.service.exception.GameException;

/**
 * Listener that is triggered when the player as played
 * 
 * @author Aurélien Baudet
 *
 */
public interface GameRunner {
	/**
	 * Fired when an action is played for the game.
	 * 
	 * @param game
	 *            the game that is waiting for the action
	 * @param playedAction
	 *            the action
	 * @throws GameException
	 *             when execution of the action has failed
	 */
	void next(Game game) throws GameException;

	/**
	 * Fired when a player has answered a question
	 * 
	 * @param game
	 * @param player
	 * @param response
	 * @throws GameException
	 */
	public void answered(Game game, Response response) throws GameException;
}
