package fr.sii.formation.quiz.core.service;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Round;
import fr.sii.formation.quiz.core.service.exception.GameException;

/**
 * 
 * @author nroussel
 *
 */
public interface RoundService {

	/**
	 * return the current Round og a game
	 * 
	 * @param game
	 *            the game 
	 * @throws GameException
	 *             when the game couldn't stop or already stopped
	 */
	public Round getCurrentRound(Game game) throws GameException;

}
