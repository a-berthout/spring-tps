package fr.sii.formation.quiz.core.service.impl.answer.manager;

import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.QuestionType;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.service.answer.AnswerManager;

public class StringAnswerManager implements AnswerManager {

	@Override
	public QuestionType getManagedType() {
		return QuestionType.SENTENCE;
	}

	@Override
	public boolean isStrictlyCorrect(Question question, Response response) {
		return question.getAnswer().getText().equals(response.getText());
	}

	@Override
	public boolean isAlmostCorrect(Question question, Response response) {
		return question.getAnswer().getText().trim().toLowerCase().equals(response.getText().trim().toLowerCase());
	}

}
