package fr.sii.formation.quiz.core.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Game {
	/**
	 * The game id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * The playing players
	 */
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL)
	private List<Player> players;

	/**
	 * All past and current rounds
	 */
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL)
	private List<Round> rounds;

	/**
	 * The list of all available questions
	 */
	@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL)
	private List<Question> questions;

	/**
	 * Is the game stopped?
	 */
	@Enumerated(EnumType.STRING)
	private GameState state;

	/**
	 * Default constructor for JPA
	 */
	public Game() {
		super();
	}

	/**
	 * Constructor use at the beginning of the project, for TP purpose
	 * 
	 * @param initQuestions
	 */
	public Game(Boolean initQuestions) {
		this();
		if (initQuestions) {
			initQuestions();
			setState(GameState.READY);
		}
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the rounds
	 */
	public List<Round> getRounds() {
		return rounds;
	}

	/**
	 * @param rounds
	 *            the rounds to set
	 */
	public void setRounds(List<Round> rounds) {
		this.rounds = rounds;
	}

	/**
	 * @return the players
	 */
	public List<Player> getPlayers() {
		return players;
	}

	/**
	 * @param players
	 *            the players to set
	 */
	public void setPlayers(List<Player> players) {
		this.players = players;
	}

	/**
	 * @return the questions
	 */
	public List<Question> getQuestions() {
		return questions;
	}

	/**
	 * @param questions
	 *            the questions to set
	 */
	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	/**
	 * @return the state
	 */
	public GameState getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(GameState state) {
		this.state = state;
	}

	public int getCurrentRound() {
		return (rounds != null) ? rounds.size() : 0;
	}

	/**
	 * Initialize the questions, only used for TP purposes
	 */
	private void initQuestions() {
		List<Question> questions = new ArrayList<>();
		questions.add(new Question("Combien de genoux un éléphant a-t-il ?", new Answer("4"), QuestionType.INTEGER));
		questions.add(new Question("Sous quel nom la Société des Papiers Linges est-elle connue ?",
				new Answer("sopalin"), QuestionType.SENTENCE));
		questions.add(new Question("Combien de pull-over peut-on faire avec un mouton ?", new Answer("14"),
				QuestionType.INTEGER));
		questions.add(new Question("Combien il y a-t-il de façons différentes de rendre la monnaie sur 1 dollar ?",
				new Answer("293"), QuestionType.INTEGER));
		questions.add(
				new Question("Quel est le record de durée du plus long vol d'avion en papier ? (arrondi à la seconde)",
						new Answer("28"), QuestionType.INTEGER));
		questions.add(new Question("Combien faut-il de muscles différents à un humain pour parler ?", new Answer("72"),
				QuestionType.INTEGER));
		setQuestions(questions);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Game [id=" + id + ", currentTurn=" + getCurrentRound() + ", players=" + players + ", questions="
				+ questions + ", state=" + state + "]";
	}

}
