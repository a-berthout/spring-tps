package fr.sii.formation.quiz.core.config;

public class GameProperties {
	/**
	 * Automatically start the game if enough teams in the game. Automatically
	 * does not start by default.
	 */
	private boolean autoStart = false;

	/**
	 * The minimum number of players to be able to play the game. Default to 1
	 * player
	 */
	private int minPlayers = 1;

	/**
	 * The minimum number of questions to be able to play the game. Default to
	 * 5 questions
	 */
	private int minQuestions = 5;


	public GameProperties() {
		super();
	}

	public GameProperties(boolean autoStart, int minPlayers, int minQuestions) {
		super();
		this.autoStart = autoStart;
		this.minPlayers = minPlayers;
		this.minQuestions = minQuestions;
	}

	/**
	 * @return the autoStart
	 */
	public boolean isAutoStart() {
		return autoStart;
	}

	/**
	 * @param autoStart the autoStart to set
	 */
	public void setAutoStart(boolean autoStart) {
		this.autoStart = autoStart;
	}

	/**
	 * @return the minPlayers
	 */
	public int getMinPlayers() {
		return minPlayers;
	}

	/**
	 * @param minPlayers the minPlayers to set
	 */
	public void setMinPlayers(int minPlayers) {
		this.minPlayers = minPlayers;
	}

	/**
	 * @return the minQuestions
	 */
	public int getMinQuestions() {
		return minQuestions;
	}

	/**
	 * @param minQuestions the minQuestions to set
	 */
	public void setMinQuestions(int minQuestions) {
		this.minQuestions = minQuestions;
	}

	
}
