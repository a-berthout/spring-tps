package fr.sii.formation.quiz.core.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Round {
	/**
	 * The round id
	 */
	@Id
	@GeneratedValue
	private Long id;

	/**
	 * The responses
	 */
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL)
	private List<Response> responses;

	@OneToOne
	private Question question;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the responses
	 */
	public List<Response> getResponses() {
		return responses;
	}

	/**
	 * @param responses
	 *            the responses to set
	 */
	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}

	/**
	 * @return the question
	 */
	public Question getQuestion() {
		return question;
	}

	/**
	 * @param question
	 *            the question to set
	 */
	public void setQuestion(Question question) {
		this.question = question;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Round [id=" + id + ", responses=" + responses + ", question=" + question + "]";
	}

}
