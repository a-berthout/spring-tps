package fr.sii.formation.quiz.core.service.impl;

import java.util.List;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Round;
import fr.sii.formation.quiz.core.service.RoundService;
import fr.sii.formation.quiz.core.service.exception.GameException;

public class SimpleRoundService implements RoundService {

	@Override
	public Round getCurrentRound(Game game) throws GameException {
		List<Round> rounds = game.getRounds();
		if (null == rounds || rounds.isEmpty()) {
			throw new GameException("No round have been created");
		} else {
			return rounds.get(rounds.size() - 1);
		}
	}

}
