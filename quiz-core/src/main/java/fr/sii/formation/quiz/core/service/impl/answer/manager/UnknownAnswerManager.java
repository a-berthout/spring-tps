package fr.sii.formation.quiz.core.service.impl.answer.manager;

import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.QuestionType;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.service.answer.AnswerManager;

public class UnknownAnswerManager implements AnswerManager {

	@Override
	public QuestionType getManagedType() {
		return null;
	}

	@Override
	public boolean isStrictlyCorrect(Question question, Response response) {
		return false;
	}

	@Override
	public boolean isAlmostCorrect(Question question, Response response) {
		return false;
	}

}
