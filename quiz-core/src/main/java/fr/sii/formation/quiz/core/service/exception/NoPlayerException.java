package fr.sii.formation.quiz.core.service.exception;

public class NoPlayerException extends GameException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 43673803569976L;

	public NoPlayerException(String message) {
		super(message);
	}

	public NoPlayerException(Throwable cause) {
		super(cause);
	}

	public NoPlayerException(String message, Throwable cause) {
		super(message, cause);
	}

}