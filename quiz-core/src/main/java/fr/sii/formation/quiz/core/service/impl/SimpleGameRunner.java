package fr.sii.formation.quiz.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.GameState;
import fr.sii.formation.quiz.core.domain.Player;
import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.domain.Round;
import fr.sii.formation.quiz.core.service.DisplayService;
import fr.sii.formation.quiz.core.service.GameRunner;
import fr.sii.formation.quiz.core.service.PromptService;
import fr.sii.formation.quiz.core.service.QuestionService;
import fr.sii.formation.quiz.core.service.RoundService;
import fr.sii.formation.quiz.core.service.answer.AnswerValidator;
import fr.sii.formation.quiz.core.service.exception.GameException;
import fr.sii.formation.quiz.core.service.exception.NoPlayerException;
import fr.sii.formation.quiz.core.service.exception.NoQuestionException;
import fr.sii.formation.quiz.core.service.exception.PlayerNotFound;

public class SimpleGameRunner implements GameRunner {
	private QuestionService questionService;
	private PromptService promptService;
	private DisplayService displayService;
	private RoundService roundService;
	private AnswerValidator answerValidator;

	public SimpleGameRunner(QuestionService questionService, PromptService promptService, DisplayService displayService,
			RoundService roundService, AnswerValidator answerValidator) {
		super();
		this.questionService = questionService;
		this.promptService = promptService;
		this.displayService = displayService;
		this.roundService = roundService;
		this.answerValidator = answerValidator;
	}

	@Override
	public void next(Game game) throws GameException {
		if (!GameState.PLAYING.equals(game.getState())) {
			throw new IllegalStateException(
					"Can't do next on game that is not currently playing (id=" + game.getId() + ")");
		}
		game = reloadGame(game);
		try {
			// Get new question
			Question question = questionService.getNextQuestion(game);
			// increment turn
			Round currentRound = new Round();
			currentRound.setQuestion(question);
			if (game.getRounds() == null) {
				game.setRounds(new ArrayList<Round>());
			}
			game.getRounds().add(currentRound);
			// update the game
			saveGame(game);
			// prompt each player of each team which action to play
			boolean waitForAction = false;
			for (Player player : game.getPlayers()) {
				player = reloadPlayer(player);
				Response response = promptService.ask(game, player, question);
				if (response != null) {
					answered(game, response);
				}
				waitForAction = true;
			}
			if (!waitForAction) {
				execute(game);
			}
		} catch (NoQuestionException e) {
			game.setState(GameState.FINISHED);
		}
	}

	@Override
	public void answered(Game game, Response response) throws GameException {
		game = reloadGame(game);
		if (!GameState.PLAYING.equals(game.getState())) {
			throw new IllegalStateException(
					"Can't execute played actions on game that is not currently playing (id=" + game.getId() + ")");
		}
		// Check that response has a player and recover it from game (Mandatory for the first TPs)
		if (null == response.getPlayer() || null == response.getPlayer().getId()) {
			throw new NoPlayerException("Response has no player associated.");
		}
		Player player = game.getPlayers().stream()
			.filter(p -> response.getPlayer().getId().equals(p.getId()))
			.findFirst().orElseThrow(() -> new PlayerNotFound("User not found in this game."));
		response.setPlayer(player);

		// set the current response for the player
		if (roundService.getCurrentRound(game).getResponses() == null) {
			roundService.getCurrentRound(game).setResponses(new ArrayList<>());
		}
		roundService.getCurrentRound(game).getResponses().add(response);
		saveGame(game);
		// if all players (that can play) have played
		if (haveAllPlayersPlayed(game)) {
			manageScores(game);
			execute(game);
		}
	}

	private void execute(Game game) throws GameException {
		// display executed actions
		game = reloadGame(game);
		displayService.display(game, roundService.getCurrentRound(game));
		// display new state of the game
		displayService.display(game);
		// update the game
		saveGame(game);
		// next
		next(game);
	}

	private void manageScores(Game game) throws GameException {
		Round round = roundService.getCurrentRound(game);
		for (Response response : round.getResponses()) {
			if (answerValidator.isCorrect(round.getQuestion(), response)) {
				Player player = response.getPlayer();
				player = reloadPlayer(player);
				player.setScore(player.getScore() + 1);
				savePlayer(player);
			}
		}
	}

	private boolean haveAllPlayersPlayed(Game game) throws GameException {
		// refresh game from database
		game = reloadGame(game);
		List<Player> players = game.getPlayers();
		Round currentRound = roundService.getCurrentRound(game);
		boolean found;
		for (Player player : players) {
			found = false;
			for (Response response : currentRound.getResponses()) {
				if (null == response.getPlayer()) {
					throw new NoPlayerException("Response has no player associated.");
				}
				if (response.getPlayer().getId() == player.getId()) {
					found = true;
					break;
				}
			}
			if (!found)
				return false;
		}
		return true;
	}

	protected Game reloadGame(Game game) {
		return game;
	}

	protected Player reloadPlayer(Player player) {
		return player;
	}

	protected Game saveGame(Game game) {
		return game;
	}

	protected Player savePlayer(Player player) {
		return player;
	}

}
