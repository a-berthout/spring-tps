package fr.sii.formation.quiz.core.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author nroussel
 *
 */
@Entity
public class Player {
	@Id
	@GeneratedValue
	private Long id;

	private String name;

	private Integer score;

	/**
	 * Default constructor for JPA
	 */
	public Player() {
		super();
	}

	public Player(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.score = 0;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the score
	 */
	public Integer getScore() {
		return score;
	}

	/**
	 * @param score
	 *            the score to set
	 */
	public void setScore(Integer score) {
		this.score = score;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name + ": " + score + " points";
	}

}
