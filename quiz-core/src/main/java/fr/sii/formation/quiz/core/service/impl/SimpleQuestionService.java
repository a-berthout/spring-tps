package fr.sii.formation.quiz.core.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.service.QuestionService;
import fr.sii.formation.quiz.core.service.exception.NoQuestionException;

public class SimpleQuestionService implements QuestionService {

	private Map<Long, Integer> lastIndexes = new HashMap<>();

	@Override
	public List<Question> getAvailableQuestions(Game game) {
		return game.getQuestions();
	}

	@Override
	public Question getNextQuestion(Game game) throws NoQuestionException {
		if (!lastIndexes.containsKey(game.getId())) {
			if (game.getRounds() != null && !game.getRounds().isEmpty()) {
				lastIndexes.put(game.getId(), game.getRounds().size());
			} else {
				lastIndexes.put(game.getId(), 0);
			}
		}
		try {
			Integer index = lastIndexes.get(game.getId());
			Question question = game.getQuestions().get(index);
			lastIndexes.put(game.getId(), index + 1);
			return question;
		} catch (IndexOutOfBoundsException e) {
			throw new NoQuestionException("No question anymore: Game Over");
		}
	}

}
