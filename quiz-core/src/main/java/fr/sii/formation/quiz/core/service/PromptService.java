package fr.sii.formation.quiz.core.service;

import fr.sii.formation.quiz.core.domain.Game;
import fr.sii.formation.quiz.core.domain.Player;
import fr.sii.formation.quiz.core.domain.Question;
import fr.sii.formation.quiz.core.domain.Response;
import fr.sii.formation.quiz.core.service.exception.PromptException;

public interface PromptService {
	
	/**
	 * Ask to the player a question
	 * 
	 * @param game
	 *            the current game
	 * @param player
	 *            the player
	 * @param question
	 *            the question
	 */
	Response ask(Game game, Player player, Question question) throws PromptException;

}
